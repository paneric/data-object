<?php

declare(strict_types=1);

namespace Paneric\DataObject;

use Paneric\Interfaces\DataObject\DataObjectInterface;

abstract class DAO extends DataObject implements DataObjectInterface
{
    protected $prefix;//string

    protected $createdAt;//string
    protected $updatedAt;//string


    public function __set($scKey, $value)
    {
        if ($value === null) {
            return;
        }

        $scKey = str_replace($this->prefix, '', $scKey);

        if (in_array($scKey, $this->scMap, true)) {
            $ccKey = $scKey;

            if (strpos($scKey, '_') !== false) {
                $ccKey = str_replace(' ', '', ucwords(str_replace('_', ' ', $scKey)));
            }

            $this->{'set' . ucfirst($ccKey)}($value);
        }
    }


    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }
    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }
    public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
