<?php

declare(strict_types=1);

namespace Paneric\DataObject\Example;

use Paneric\DataObject\ADAO;

class Address extends ADAO
{
    protected null|int|string $id;
    protected null|int|string $listCountryId;
    protected null|int|string $listTypeCompanyId;

    protected null|ListCountry $listCountry;
    protected null|ListTypeCompany $listTypeCompany;

    protected null|string $ref;
    protected null|string $street;
    protected null|string $city;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'adr_';

        $this->setMaps();

        if ($this->values) {
            $this->listCountry = new ListCountry($this->values);
            $this->listTypeCompany = new ListTypeCompany($this->values);

            $this->hydrate($this->values);

            unset($this->values);
        }
    }


    public function getId(): null|int|string
    {
        return $this->id;
    }
    public function getListCountryId(): null|int|string
    {
        return $this->listCountryId;
    }
    public function getListTypeCompanyId(): null|int|string
    {
        return $this->listTypeCompanyId;
    }


    public function getListCountry(): null|ListCountry
    {
        return $this->listCountry;
    }
    public function getListTypeCompany(): null|ListTypeCompany
    {
        return $this->listTypeCompany;
    }


    public function getRef(): null|string
    {
        return $this->ref;
    }
    public function getStreet(): null|string
    {
        return $this->street;
    }
    public function getCity(): null|string
    {
        return $this->city;
    }


    public function setId(null|int|string $id): void
    {
        $this->id = $id;
    }
    public function setListCountryId(null|int|string $listCountryId): void
    {
        $this->listCountryId = $listCountryId;
    }
    public function setListTypeCompanyId(null|int|string $listTypeCompanyId): void
    {
        $this->listTypeCompanyId = $listTypeCompanyId;
    }


    public function setListCountry(null|array|ListCountry $listCountry): void
    {
        $this->listCountry = $listCountry;
    }
    public function setListTypeCompany(null|array|ListTypeCompany $listTypeCompany): void
    {
        $this->listTypeCompany = $listTypeCompany;
    }


    public function setRef(null|string $ref): void
    {
        $this->ref = $ref;
    }
    public function setStreet(null|string $street): void
    {
        $this->street = $street;
    }
    public function setCity(null|string $city): void
    {
        $this->city = $city;
    }
}
