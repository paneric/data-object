<?php

declare(strict_types=1);

namespace Paneric\DataObject\Example;

use Paneric\DataObject\ADAO;

class ListCountry extends ADAO
{
    protected null|int|string $id;
    protected string $ref;
    protected string $label;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'lc_';

        $this->setMaps();

        if ($this->values) {
            $this->hydrate($this->values);

            unset($this->values);
        }
    }


    public function getId(): null|int|string
    {
        return $this->id;
    }
    public function getRef(): null|string
    {
        return $this->ref;
    }
    public function getlabel(): null|string
    {
        return $this->label;
    }


    public function setId(null|int|string $id): void
    {
        $this->id = $id;
    }
    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }
}