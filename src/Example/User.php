<?php

declare(strict_types=1);

namespace Paneric\DataObject\Example;

use Paneric\DataObject\ADAO;

class User extends ADAO
{
    protected null|int|string $id;
    protected null|int|string $credentialId;

    protected null|Credential $credential;

    protected null|string $ref;
    protected null|string $name;
    protected null|string $surname;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'usr_';

        $this->setMaps();

        if ($this->values) {
            $this->credential = new Credential($this->values);

            $this->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): null|int|string
    {
        return $this->id;
    }
    public function getCredentialId(): null|int|string
    {
        return $this->credentialId;
    }


    public function getCredential(): null|Credential
    {
        return $this->credential;
    }


    public function getRef(): null|string
    {
        return $this->ref;
    }
    public function getName(): null|string
    {
        return $this->name;
    }
    public function getSurname(): null|string
    {
        return $this->surname;
    }


    public function setId(null|int|string $id): void
    {
        $this->id = $id;
    }
    public function setCredentialId(null|int|string $credentialId): void
    {
        $this->credentialId = $credentialId;
    }


    public function setCredential(null|Credential $credential): void
    {
        $this->credential = $credential;
    }


    public function setRef(null|string $ref): void
    {
        $this->ref = $ref;
    }
    public function setName(null|string $name): void
    {
        $this->name = $name;
    }
    public function setSurname(null|string $surname): void
    {
        $this->surname = $surname;
    }
}
