<?php

namespace Paneric\DataObject\Example;

use Paneric\DataObject\ADAO;

class Credential extends ADAO
{
    protected null|int|string $id;
    protected string $ref;
    protected string $email;
    protected string $gsm;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'crd_';

        $this->setMaps();

        if ($this->values) {
            $this->hydrate($this->values);

            unset($this->values);
        }
    }


    public function getId(): null|int|string
    {
        return $this->id;
    }
    public function getRef(): null|string
    {
        return $this->ref;
    }
    public function getEmail(): null|string
    {
        return $this->email;
    }
    public function getGsm(): null|string
    {
        return $this->gsm;
    }


    public function setId(null|int|string $id): void
    {
        $this->id = $id;
    }
    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
    public function setGsm(string $gsm): void
    {
        $this->gsm = $gsm;
    }
}
