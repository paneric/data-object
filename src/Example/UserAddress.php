<?php

declare(strict_types=1);

namespace Paneric\DataObject\Example;

use Paneric\DataObject\ADAO;

class UserAddress extends ADAO
{
    protected null|int|string $id;
    protected null|int|string $userId;
    protected null|int|string $addressId;

    protected null|User $user;
    protected null|Address $address;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'usad_';

        $this->setMaps();

        if ($this->values) {
            $this->user = new User($this->values);
            $this->address = new Address($this->values);

            $this->hydrate($this->values);

            unset($this->values);
        }
    }


    public function getId(): null|int|string
    {
        return $this->id;
    }
    public function getUserId(): null|int|string
    {
        return $this->userId;
    }
    public function getAddressId(): null|int|string
    {
        return $this->addressId;
    }


    public function getUser(): null|User
    {
        return $this->user;
    }
    public function getAddress(): null|Address
    {
        return $this->address;
    }


    public function setId(null|int|string $id): void
    {
        $this->id = $id;
    }
    public function setUserId(null|int|string $userId): void
    {
        $this->userId = $userId;
    }
    public function setAddressId(null|int|string $addressId): void
    {
        $this->addressId = $addressId;
    }


    public function setUser(null|User $user): void
    {
        $this->user = $user;
    }
    public function setAddress(null|Address $address): void
    {
        $this->address = $address;
    }
}
