<?php

declare(strict_types=1);

namespace Paneric\DataObject;

use Paneric\Interfaces\DataObject\DataObjectInterface;

abstract class DataObject implements DataObjectInterface
{
    protected null|array $ccMap = [];
    protected null|array $scMap = [];

    protected function setMaps(): void
    {
        $attributes = get_class_vars(get_class($this));

        unset($attributes['prefix'], $attributes['ccMap'], $attributes['scMap'], $attributes['values']);

        $this->ccMap = array_keys($attributes);

        foreach ($this->ccMap as $ccItem) {
            $this->scMap[] = strtolower(
                preg_replace('/([a-z])([A-Z])/', '$1_$2', $ccItem)
            );
        }
    }

    public function hydrate(array $attributes, bool $isSc = true): array
    {
        if (empty($this->ccMap)) {
            $this->setMaps();
        }

        if ($isSc) {
            return $this->hydrateSc($attributes);
        }

        return $this->hydrateCc($attributes);
    }

    protected function hydrateSc(array $attributes): array
    {
        foreach ($attributes as $scKey => $value) {
            $key = str_replace($this->prefix, '', $scKey);
            if (in_array($key, $this->scMap, true)) {
                if ($value === 0 || $value === 0.0 || !in_array($value, ['', [], null], true)) {
                    $i = array_search($key, $this->scMap, true);

                    $this->{'set' . ucfirst($this->ccMap[$i])}($value);
                }

                unset($attributes[$scKey]);
            }
        }

        return $attributes;
    }

    protected function hydrateCc(array $attributes): array
    {
        foreach ($attributes as $ccKey => $value) {
            if (in_array($ccKey, $this->ccMap, true)) {
                if ($value === 0 || $value === 0.0 || !in_array($value, ['', [], null], true)) {
                    $this->{'set' . ucfirst($ccKey)}($value);
                }
            }

            unset($attributes[$ccKey]);
        }

        return $attributes;
    }

    public function convert(bool $isSc = true): array
    {
        if ($this->ccMap === null) {
            $this->setMaps();
        }

        if ($isSc) {
            return $this->convertSc();
        }

        return $this->convertCc();
    }

    protected function convertSc(): array
    {
        $attributes = [];

        foreach ($this->ccMap as $i => $ccKey) {
            if (isset($this->$ccKey) && !in_array($ccKey, ['scMap', 'ccMap'])) {
                if ($this->$ccKey instanceof DataObjectInterface) {
                    $attributes[$this->scMap[$i]] = $this->{lcfirst($ccKey)}->convert();
                    continue;
                }
                $attributes[$this->scMap[$i]] = $this->{lcfirst($ccKey)};
            }
        }

        return $attributes;
    }

    protected function convertCc(): array
    {
        $attributes = [];

        foreach ($this->ccMap as $ccKey) {
            if ($this->$ccKey !== null && !in_array($ccKey, ['scMap', 'ccMap'])) {
                if ($this->$ccKey instanceof DataObjectInterface) {
                    $attributes[$ccKey] = $this->{lcfirst($ccKey)}->convert(false);
                    continue;
                }
                $attributes[$ccKey] = $this->{lcfirst($ccKey)};
            }
        }

        return $attributes;
    }

    public function prepare(bool $unsetId = false): array
    {
        $scKeys = $this->scMap;

        $scKey1 = array_search('created_at', $scKeys, true);
        $scKey2 = array_search('updated_at', $scKeys, true);

        unset($scKeys[$scKey1], $scKeys[$scKey2]);

        $attributes = [];

        foreach ($scKeys as $i => $scKey) {
            $attributeValue = $this->{$this->ccMap[$i]} ?? null;

            if ($attributeValue !== null && !is_object($attributeValue)) {
                $attributes[$this->prefix . $scKey] = $attributeValue;
            }
        }

        if ($unsetId) {
            unset($attributes[$this->prefix . 'id']);
        }

        return $attributes;
    }
}
