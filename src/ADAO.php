<?php

declare(strict_types=1);

namespace Paneric\DataObject;

abstract class ADAO extends DataObject
{
    protected string $prefix;

    protected string $createdAt;
    protected string $updatedAt;

    protected array $values = [];

    public function __construct(array $values = null)
    {
        if ($values !== null) {
            $this->values = $values;
        }
    }

    public function __set($scKey, $value)
    {
        $this->values[$scKey] = $value;
    }
    public function __get(string $name)
    {
        return null;
    }
    public function __isset(string $name)
    {
        return null;
    }


    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }
    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }


    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
    public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
