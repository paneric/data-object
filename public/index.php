<?php

declare(strict_types=1);

use Paneric\DataObject\Example\UserAddress;

const ENV = 'dev';

define('ROOT_FOLDER', dirname(__DIR__) . '/');
const APP_FOLDER = ROOT_FOLDER . 'src/';

require ROOT_FOLDER . 'vendor/autoload.php';

$attributes = [
    'lc_id' => 'lc_id',
    'lc_ref' => 'lc_ref',
    'lc_label' => 'lc_label',

    'ltc_id' => 'ltc_id',
    'ltc_ref' => 'ltc_ref',
    'ltc_label' => 'ltc_label',

    'crd_id' => 'crd_id',
    'crd_ref' => 'crd_ref',
    'crd_email' => 'crd_email',
    'crd_gsm' => 'crd_gsm',


    'adr_id' => 'adr_id',
    'adr_ref' => 'adr_ref',
    'adr_list_type_company_id' => 'ltc_id',
    'adr_list_country_id' => 'lc_id',
    'adr_street' => 'adr_street',
    'adr_city' => 'adr_city',


    'usr_id' => 'usr_id',
    'usr_ref' => 'usr_ref',
    'usr_credential_id' => 'crd_id',
    'usr_name' => 'usr_name',
    'usr_surname' => 'usr_surname',


    'usad_id' => 'usad_id',
    'usad_user_id' => 'usr_id',
    'usad_address_id' => 'adr_id',
];

$userAddress = new UserAddress($attributes);
dump($userAddress->convert());